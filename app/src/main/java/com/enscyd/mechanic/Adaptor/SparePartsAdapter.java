package com.enscyd.mechanic.Adaptor;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.enscyd.mechanic.Activities.AddSparePartsActivity;
import com.enscyd.mechanic.Model.SparePartsModel;
import com.enscyd.mechanic.R;
import java.util.ArrayList;

public class SparePartsAdapter extends RecyclerView.Adapter<SparePartsAdapter.SwipeViewHolder> {
    ArrayList<SparePartsModel> list;
    Context context;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();


    public SparePartsAdapter(Context context, ArrayList<SparePartsModel> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public SwipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SwipeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spare_parts_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull SwipeViewHolder holder, int position) {
        final SparePartsModel showPassengerModel = list.get(position);
        viewBinderHelper.setOpenOnlyOne(true);
        viewBinderHelper.bind(holder.swipelayout, showPassengerModel.getProduct_name());
        viewBinderHelper.closeLayout(showPassengerModel.getProduct_name());
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddSparePartsActivity.class);
                context.startActivity(intent);

            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class SwipeViewHolder extends RecyclerView.ViewHolder {

        private TextView productNameEt;
        private TextView productNumberEt;
        private TextView priceEt;
        private TextView productCompanyEt;
        private TextView productQualityEt;
        private TextView productQuantityEt;
        private TextView discount;

        private TextView txtEdit;
        private TextView txtDelete;
        private SwipeRevealLayout swipelayout;

        SwipeViewHolder(View itemView) {
            super(itemView);
            initView(itemView);

        }

        private void initView(View itemView) {
            productNameEt = itemView.findViewById(R.id.product_name_et);
            productNumberEt = itemView.findViewById(R.id.product_number_et);
            priceEt = itemView.findViewById(R.id.price_et);
            productCompanyEt = itemView.findViewById(R.id.product_company_et);
            productQualityEt = itemView.findViewById(R.id.product_quality_et);
            productQuantityEt = itemView.findViewById(R.id.product_quantity_et);
            discount = itemView.findViewById(R.id.discount);
            txtEdit = itemView.findViewById(R.id.txtEdit);
            txtDelete = itemView.findViewById(R.id.txtDelete);
            swipelayout = itemView.findViewById(R.id.swipelayout);


        }
    }


}
