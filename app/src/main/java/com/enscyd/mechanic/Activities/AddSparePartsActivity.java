package com.enscyd.mechanic.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.enscyd.mechanic.Helper.Config;
import com.enscyd.mechanic.Helper.ImageUtils;
import com.enscyd.mechanic.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.enscyd.mechanic.Helper.Config.askPermissions;

public class AddSparePartsActivity extends AppCompatActivity {

    private int CAPTURE_IMAGE = 222;
    private Button takeBtn, pickPhotoBtn;
    private static int PICK_IMAGE = 1;
    String imageName = "product_image";
    private ImageView postImageview;
    private EditText productNameEt;
    private EditText productNumberEt;
    private EditText productCompanyEt;
    private EditText productQualityEt;
    private EditText discount;
    private EditText priceEt;
    private AppCompatButton addBtn;
    private View view3;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_spare_parts);
        initView();
        context = this;
        askPermissions(context);

        pickPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    selectGalleryImage();
                }

            }
        });


        takeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    captureImage();
                }

            }
        });


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);


        if (resultCode == RESULT_OK && requestCode == CAPTURE_IMAGE) {
            Bitmap myBitmap = ImageUtils.getInstant().getCompressedBitmap(sdImageMainDirectory.getAbsolutePath());

            postImageview.setImageBitmap(myBitmap);
            saveImage(myBitmap, imageName);
            // sent pic to firebase server

        }


        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {


//            Bitmap originalPhoto = AfterAcquisition(requestCode, resultCode, data);
            Bitmap myBitmap = ImageUtils.getInstant().getCompressedBitmap(getRealPathFromURI(context, data.getData()));
            postImageview.setImageBitmap(myBitmap);
            saveImage(myBitmap, imageName);
// sent pic to firebase server


        }

    }

    private void saveImage(Bitmap bmp, String filename) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 40, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Uri outputFileUri;

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("getRealPathFromURI", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    File sdImageMainDirectory;

    private void captureImage() {
        imageName = "img" + System.currentTimeMillis() + ".png";

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sdImageMainDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName);

        try {
            sdImageMainDirectory.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        outputFileUri = Uri.fromFile(sdImageMainDirectory);


        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(captureIntent, CAPTURE_IMAGE);
    }

    private void selectGalleryImage() {
        try {
            imageName = "img" + System.currentTimeMillis() + ".png";
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PICK_IMAGE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        postImageview = findViewById(R.id.post_imageview);
        productNameEt = findViewById(R.id.product_name_et);
        productNumberEt = findViewById(R.id.product_number_et);
        productCompanyEt = findViewById(R.id.product_company_et);
        productQualityEt = findViewById(R.id.product_quality_et);
        discount = findViewById(R.id.discount);
        priceEt = findViewById(R.id.price_et);
        takeBtn = findViewById(R.id.take_btn);
        pickPhotoBtn = findViewById(R.id.pick_photo_btn);
        addBtn = findViewById(R.id.add_btn);
        view3 = findViewById(R.id.view3);
    }
}
