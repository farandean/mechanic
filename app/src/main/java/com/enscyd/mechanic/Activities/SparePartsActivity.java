package com.enscyd.mechanic.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.enscyd.mechanic.Adaptor.SparePartsAdapter;
import com.enscyd.mechanic.Model.SparePartsModel;
import com.enscyd.mechanic.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class SparePartsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FloatingActionButton fabAdd;
    Context context;
    SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<SparePartsModel> sparepartsModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spare_parts);
        initView();
        context = this;
        sparepartsModelList = new ArrayList<>();

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddSparePartsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.img_plant_1);

        SparePartsModel sparePartsModel = new SparePartsModel("1", "Tyer", "00129", "Honda",
                "100", "Very Good And Cool", icon, "50%", "1000");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);

            }
        });
        for (int i = 0; i < 15; i++) {
            sparepartsModelList.add(sparePartsModel);
        }

        SparePartsAdapter adapter = new SparePartsAdapter(context, sparepartsModelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                fabAdd.setVisibility(View.VISIBLE);

            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    fabAdd.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initView() {
        recyclerView = findViewById(R.id.recycler_view);
        fabAdd = findViewById(R.id.fab_add);
        swipeRefreshLayout = findViewById(R.id.swipelayout);
    }
}
