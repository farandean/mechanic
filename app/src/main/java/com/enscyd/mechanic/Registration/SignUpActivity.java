package com.enscyd.mechanic.Registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.enscyd.mechanic.Activities.MainActivity;
import com.enscyd.mechanic.Activities.MapsActivity;
import com.enscyd.mechanic.Helper.Config;
import com.enscyd.mechanic.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private EditText nameEt;
    private EditText cnicEt;
    private EditText phoneEt;
    private EditText emailEt;
    private EditText passwordEt;
    private TextView mapEt;
    private TextView registerBtn;
    private AppCompatButton loginBtn;
    private static int PICK_IMAGE = 1;
    Context context;
    private final int REQUEST_LOCATION_PERMISSION = 125;
    String imageName = "product_image";
    private String lat = "0.0";
    private String lng = "0.0";
    //private Button takeBtn;
    private Button pickPhotoBtn;
    private int CAPTURE_IMAGE = 222;
    private Bitmap bitmap;
    private Uri filePath;
    private Boolean image_Selected = false;
    private ImageView postImageview;
    private EditText confirmPasswordEt;
    private String image_in_string = null;
    private String name, shopName, password, email, phone, cnic, address, confirmPassword;
    private EditText shopNameEt;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();
        context = SignUpActivity.this;
        mAuth= FirebaseAuth.getInstance();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameEt.getText().toString();
                phone = phoneEt.getText().toString();
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();
                confirmPassword = confirmPasswordEt.getText().toString();
                cnic = cnicEt.getText().toString();
                address = mapEt.getText().toString();
                shopName =shopNameEt.getText().toString();
                if (name.matches("") || phone.matches("") || email.matches("")
                        || password.matches("") || confirmPassword.matches("")
                        || cnic.matches("") || address.matches("")|| shopName.matches(""))
                {
                    Toast.makeText(context, "Complete All Fields ...", Toast.LENGTH_SHORT).show();
                } else if (image_in_string == null) {
                    Toast.makeText(context, "Please Select Image ...", Toast.LENGTH_SHORT).show();
                } else {

                    Config.showDialog("Create Account",context);
                    mAuth.createUserWithEmailAndPassword(concat(email),password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Config.hideDialog();
                            if(!task.isSuccessful())
                            {
                                Toast.makeText(SignUpActivity.this, "SignUp Failed!!! \n "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                Config.hideDialog();
                            }
                            else
                            {
                                DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference().child("Mechanics");
                                final String c_id= FirebaseAuth.getInstance().getCurrentUser().getUid();
                                Map map=new HashMap();
                                map.put("name",name);
                                map.put("shopName",shopName);
                                map.put("email",email);
                                map.put("phone",phone);
                                map.put("cnic",cnic);
                                map.put("address",address);
                                map.put("image",image_in_string);
                                map.put("lat",lat);
                                map.put("lng",lng);

                                databaseReference.child(c_id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(c_id!=null) {
                                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                            Toast.makeText(SignUpActivity.this, "SignUp Successfully !!!", Toast.LENGTH_SHORT).show();
                                            Config.hideDialog();
                                            finish();
                                        }
                                        else
                                        {
                                            startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                                            finish();
                                            Toast.makeText(SignUpActivity.this, "SignUp Successfully !!!", Toast.LENGTH_SHORT).show();
                                            Config.hideDialog();
                                        }
                                    }
                                });

                            }
                        }
                    });
                }
            }
        });



        mapEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SignUpActivity.this, MapsActivity.class), REQUEST_LOCATION_PERMISSION);

            }
        });


        pickPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    selectGalleryImage();
                }

            }
        });


       /* takeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    captureImage();
                }

            }
        });*/


    }
    private String concat(String email) {
        email="mech_"+email;
        return email;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (resultCode == RESULT_OK) {


                String address = data.getStringExtra("address");
                lat = data.getStringExtra("lat");
                lng = data.getStringExtra("lng");
                Log.d("lat", lat + "");
                Log.d("lng", lng + "");
                mapEt.setText(address);
            }
        } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(context).load(bitmap).into(postImageview);
                image_in_string = getStringImage(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void saveImage(Bitmap bmp, String filename) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 40, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Uri outputFileUri;

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("getRealPathFromURI", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    File sdImageMainDirectory;

    private void captureImage() {
        imageName = "img" + System.currentTimeMillis() + ".png";

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sdImageMainDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName);

        try {
            sdImageMainDirectory.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        outputFileUri = Uri.fromFile(sdImageMainDirectory);


        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(captureIntent, CAPTURE_IMAGE);
    }

    private void selectGalleryImage() {
        try {
            //imageName = "img" + System.currentTimeMillis() + ".png";
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PICK_IMAGE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        nameEt = findViewById(R.id.name_et);
        cnicEt = findViewById(R.id.cnic_et);
        phoneEt = findViewById(R.id.phone_et);
        emailEt = findViewById(R.id.email_et);
        passwordEt = findViewById(R.id.password_et);
        mapEt = findViewById(R.id.map_et);
        registerBtn = findViewById(R.id.register_btn);
        loginBtn = findViewById(R.id.login_btn);
        //takeBtn = findViewById(R.id.take_btn);
        pickPhotoBtn = findViewById(R.id.pick_photo_btn);
        postImageview = (ImageView) findViewById(R.id.post_imageview);
        confirmPasswordEt = (EditText) findViewById(R.id.confirm_password_et);
        shopNameEt = (EditText) findViewById(R.id.shopName_et);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
