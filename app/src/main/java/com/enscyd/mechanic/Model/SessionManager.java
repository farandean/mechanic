package com.enscyd.mechanic.Model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.enscyd.mechanic.R;
import com.enscyd.mechanic.Registration.LoginActivity;

import java.io.File;

import static android.content.Context.MODE_PRIVATE;

public class SessionManager {
    // Shared preferences file name
    private static final String PREF_NAME = "mehicane";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    // Shared Preferences
    static SharedPreferences pref;
    static Editor editor;
    // Shared pref mode
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    public static void setLogin(Context context, boolean isLoggedIn) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public static boolean isLoggedIn(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public static void setTerm(Context context, boolean isAccepted) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
        editor.putBoolean("term", isAccepted);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public static boolean isAccepted(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return pref.getBoolean("term", false);
    }

    public static void setLoginUser(Context context, UserModel user) {
        if (user.getEmail().charAt(0) != '+' && !user.getEmail().contentEquals("null")) {
            setLogin(context, false);
        }

        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("id", user.getId());
        editor.putString("name", user.getName());
        editor.putString("email", user.getEmail());
        editor.putString("adrress", user.getAdrress());
        editor.putString("cnic", user.getCnic());
        editor.putString("password", user.getPassword());
        editor.putString("phone", user.getPhone());

        // commit changes
        editor.commit();


    }

    public static UserModel getLoginUser(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String id = pref.getString("id", "Null");
        String name = pref.getString("name", "Null");
        String cnic = pref.getString("cnic", "Null");
        String email = pref.getString("email", "Null");
        String phone = pref.getString("phone", "Null");
        String password = pref.getString("password", "Null");
        String address = pref.getString("address", "Null");

        return new UserModel(id, name, address, cnic, email, password, phone);
    }

    public static void logoutUser(final Context context, int mode, final TextView name, final TextView email) {
        if (mode == 1) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("Do you want to logout as " + getLoginUser(context).getName() + " ?");
            dialog.setPositiveButton("Yes Logout", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    logout(context);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                }
            });

            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                name.setAllCaps(true);
                name.setText("Tap to login");
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        logoutUser(context, 0, name, email);
                    }
                });

                email.setText("");
                logout(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static void logout(Context context) {
        setLogin(context, false);
        setLoginUser(context, new UserModel("null", "null", "null", "null", "null", "null", "null"));
        setUsername(context);
        deleteRecursive(new File(context.getFilesDir().getPath()));
        // Launching the login activity
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(intent);
        ((AppCompatActivity) context).finish();
    }

    private static void setUsername(Context context) {
        SharedPreferences mStore = context.getSharedPreferences("Sinch", MODE_PRIVATE);
        Editor editor = mStore.edit();
        editor.putString("Username", "");
        editor.commit();
    }

    static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();

    }


}
