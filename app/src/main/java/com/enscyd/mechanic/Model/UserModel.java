package com.enscyd.mechanic.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {
    String id;
    String name;
    String adrress;
    String cnic;
    String email;
    String password;
    String phone;

    protected UserModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        adrress = in.readString();
        cnic = in.readString();
        email = in.readString();
        password = in.readString();
        phone = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(adrress);
        dest.writeString(cnic);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdrress() {
        return adrress;
    }

    public void setAdrress(String adrress) {
        this.adrress = adrress;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserModel(String id, String name, String adrress, String cnic, String email, String password, String phone) {
        this.id = id;
        this.name = name;
        this.adrress = adrress;
        this.cnic = cnic;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }
}
