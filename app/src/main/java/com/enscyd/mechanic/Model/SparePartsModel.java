package com.enscyd.mechanic.Model;

import android.graphics.Bitmap;

public class SparePartsModel {

    String id;
    String product_name;
    String product_number;
    String product_company;
    String product_quantity;
    String product_quality;
    Bitmap product_image;
    String discount;
    String fixed_price;

    public SparePartsModel(String id, String product_name, String product_number, String product_company, String product_quantity, String product_quality, Bitmap product_image, String discount, String fixed_price) {
        this.id = id;
        this.product_name = product_name;
        this.product_number = product_number;
        this.product_company = product_company;
        this.product_quantity = product_quantity;
        this.product_quality = product_quality;
        this.product_image = product_image;
        this.discount = discount;
        this.fixed_price = fixed_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_number() {
        return product_number;
    }

    public void setProduct_number(String product_number) {
        this.product_number = product_number;
    }

    public String getProduct_company() {
        return product_company;
    }

    public void setProduct_company(String product_company) {
        this.product_company = product_company;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProduct_quality() {
        return product_quality;
    }

    public void setProduct_quality(String product_quality) {
        this.product_quality = product_quality;
    }

    public Bitmap getProduct_image() {
        return product_image;
    }

    public void setProduct_image(Bitmap product_image) {
        this.product_image = product_image;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFixed_price() {
        return fixed_price;
    }

    public void setFixed_price(String fixed_price) {
        this.fixed_price = fixed_price;
    }
}
